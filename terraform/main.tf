resource "aws_vpc" "vpc" {
  cidr_block       = "172.16.0.0/16"
  tags {
    Name = "VPC"
  }
}

######################################################################
resource "aws_subnet" "PubSub" {
  vpc_id     = "${aws_vpc.vpc.id}"
  cidr_block = "172.16.1.0/24"
  availability_zone = "${var.region}a"
  tags {
    Name = "PubSubnet"
  }
}


resource "aws_subnet" "PriSub" {
  vpc_id     = "${aws_vpc.vpc.id}"
  cidr_block = "172.16.2.0/24"
  availability_zone = "${var.region}a"
  tags {
    Name = "PriSubnet"
  }
}

############################################################################
resource "aws_ebs_volume" "WebVol" {
    availability_zone = "${aws_subnet.PubSub.availability_zone}"
    size = "${var.EbsVolume}"
    tags {
        Name = "EbsVolume"
    }
}

# resource "aws_ebs_volume" "WebVol2" {
#     availability_zone = "${aws_subnet.PubSub2.availability_zone}"
#     size = "${var.EbsVolume}"
#     tags {
#         Name = "EbsVolume"
#     }
# }

resource "aws_ebs_volume" "AppVol" {
    availability_zone = "${aws_subnet.PriSub.availability_zone}"
    size = "${var.EbsVolume}"
    tags {
        Name = "EbsVolume"
    }
}


#################################################################################################
resource "aws_security_group" "sg_jenkins" {
  name = "sg_${var.jenkins_name}"
  description = "Allows all traffic"

  # SSH
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  # HTTP
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  # HTTPS
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  # Jenkins JNLP port
  ingress {
    from_port = 50000
    to_port = 50000
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  # Weave Scope port
  ingress {
    from_port = 4040
    to_port = 4040
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }
}

resource "aws_security_group" "PriSg" {
  vpc_id = "${aws_vpc.vpc.id}"
  name = "sg_test_web"
  description = "Allow traffic from public subnet"
  ingress {
    from_port = 3306
    to_port = 3306
    protocol = "tcp"
    cidr_blocks = ["172.16.1.0/24"]
  }

  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = ["172.16.1.0/24"]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["172.16.1.0/24"]
  }

  tags {
    Name = "Private SG"
  }
}


#######################################################################
data "template_file" "user_data" {
  template = "${file("templates/user_data.tpl")}"
}



data "template_file" "minkube" {
  template = "${file("templates/minikube.tpl")}"
}

resource "aws_instance" "Jenkinsinstance" {
  
  ami           = "${var.amiId}"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.PubSub.id}"
  key_name = "${var.KeyPairName}"
  security_groups = ["${aws_security_group.sg_jenkins.id}"]
  associate_public_ip_address  = true
  root_block_device {
    volume_size = "${var.RootVolume}"
  }
  ebs_block_device {
    volume_size = "${var.EbsVolume}"
    device_name = "/dev/xvda"
  }
  tags {
    Name = "Jenkinsserver"
  }
    provisioner "file" {
    connection {
      user = "ec2-user"
      host = "${aws_instance.jenkins.public_ip}"
      timeout = "1m"
      private_key = "${file("${var.key_name}.pem")}"
    }
    source = "templates/cron.sh"
    destination = "/home/ec2-user/cron.sh"
  }

  provisioner "remote-exec" {
    connection {
      user = "ec2-user"
      host = "${aws_instance.jenkins.public_ip}"
      timeout = "1m"
      private_key = "${file("${var.key_name}.pem")}"
    }
    inline = [
      "chmod +x /home/ec2-user/cron.sh",
      "/home/ec2-user/cron.sh ${var.aws_access_key} ${var.aws_secret_key} ${var.s3_bucket} ${var.jenkins_name}"
    ]
  }
}


resource "aws_instance" "minikube" {
  ami           = "${var.amiId}"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.PriSub.id}"
  key_name = "${var.KeyPairName}"
  security_groups = ["${aws_security_group.PriSg.id}"]
  root_block_device {
    volume_size = "${var.RootVolume}"
  }
  ebs_block_device {
    volume_size = "${var.EbsVolume}"
    device_name = "/dev/xvda"
  }
  tags {
    Name = "AppServer1"
  }
}


########################################################################
data "aws_elb_service_account" "main" {}
resource "aws_s3_bucket" "bucket-logs" {
  bucket = "${var.bucket_logs}"
  acl    = "log-delivery-write"
       policy = <<POLICY
{
  "Id": "Policy",
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:PutObject"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:s3:::${var.bucket_logs}/my-logs/AWSLogs/721603789875/*",
      "Principal": {
        "AWS": [
          "${data.aws_elb_service_account.main.arn}"
        ]
      }
    }
  ]
}
POLICY
}
##########################################################################################
resource "aws_elb" "PublicElb" {
  name               = "PublicElb"
  #availability_zones = ["${var.region}a, ${var.region}b"]

  access_logs {
    bucket        = "${var.bucket_logs}"
    bucket_prefix = "PublicElb"
  }
  listener {
    instance_port     = 8000
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:8000/"
    interval            = 30
  }
  instances             = ["${aws_instance.Jenkinsinstance.id}"]
  subnets  = ["${aws_subnet.PubSub.id}"]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400
  tags {
    Name = "PublicElb"
  }
}
resource "aws_elb" "PrivateElb" {
  name               = "PrivateElb"
  #availability_zones = ["${var.region}a", "${var.region}b"]
  access_logs {
    bucket        = "${var.bucket_logs}"
    bucket_prefix = "PrivateElb"
  }
  listener {
    instance_port     = 8000
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:8000/"
    interval            = 30
  }

  instances             = ["${aws_instance.minikube.id}"]
  subnets  = ["${aws_subnet.PriSub.id}"]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags {
    Name = "PrivateElb"
  }
}

###########################################################################################
resource "aws_route53_zone" "private" {
  name = "${var.hostedZone}"

  vpc {
    vpc_id = "${aws_vpc.vpc.id}"
  }
}

resource "aws_route53_record" "route" {
  zone_id = "${aws_route53_zone.private.zone_id}"
  name    = "${var.hostedZone}"
  type    = "A"
  alias {
    name                   = "${aws_elb.PublicElb.dns_name}"
    zone_id                = "${aws_elb.PublicElb.zone_id}"
    evaluate_target_health = true
  }
}

