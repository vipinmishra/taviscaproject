variable "aws_access_key" {
        description = "Access key"
        default     = ""
}

variable "aws_secret_key" {
        description = "secret key"
        default     = ""
}
variable "amiId" {
        description = "AMI ID"
        default     = "ami-0ac019f4fcb7cb7e6"
}

variable "KeyPairName" {
        description = "Key pair Name"
        default     = "demo-vpc"
}

variable "RootVolume" {
        description = "Root Volume"
        default = "10"
}

variable "EbsVolume" {
        description = "EBS Volume"
        default = "10"
}

variable "pwd" {
        description = "RDS Password"
        default = "Passw0rd"
}

variable "bucket_logs" {
        description = "Log Bucket Name"
        default = "test1"
}



variable "recordName" {
        description = "Name of Record (<recordName>.<hostedzone>) example: example.sample.com"
        default = "example"
}

variable "hostedZone" {
        description = "Name of Hosted Zone"
        default = "sample.com"
}

variable "region" {
        default = "us-east-1"
        description = "Region"
}

variable "s3_bucket" {
        description = "Bucket name"
}

variable "jenkins_name" {
        description = "jenkins name"
}



