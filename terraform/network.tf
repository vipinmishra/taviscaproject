resource "aws_internet_gateway" "ig" {
  vpc_id = "${aws_vpc.vpc.id}"
  
  tags = {
    Name = "INTERNET GATEWAY"
  }
}
#################################adding a route to internet gateway ############
resource "aws_route" "pub_route" {
  route_table_id   = "${aws_vpc.vpc.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id  = "${aws_internet_gateway.ig.id}"
}
##############################ADDING A ELASTIC IP ########################
resource "aws_eip" "eip" {
  vpc = true
  depends_on = ["aws_internet_gateway.ig"]
}
###############################CREATING A NAT GATEWAY FOR SUBNET ########
resource "aws_nat_gateway" "natpub" {
  allocation_id ="${aws_eip.eip.id}"
  subnet_id = "${aws_subnet.PubSub.id}"
  depends_on = ["aws_internet_gateway.ig"]
}

################################CREATING A PriSub1 ROUTE TABLE#############
resource "aws_route_table" "private_route_table" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name = "private subnet route table"
  }
}
###################################adding Prisub1 route to NAT###################
resource "aws_route" "private_route" {
  route_table_id = "${aws_route_table.private_route_table.id}"
  destination_cidr_block = "10.3.0.0/18"
  nat_gateway_id   = "${aws_nat_gateway.natpub.id}"
}
##################################Associate a Route table to public###############
resource "aws_route_table_association" "public_association" {
  subnet_id = "${aws_subnet.PubSub.id}"
  route_table_id = "${aws_vpc.vpc.main_route_table_id}"
}



#####################################Associate Route to private##########
resource "aws_route_table_association" "private_association" {
  subnet_id = "${aws_subnet.PriSub.id}"
  route_table_id = "${aws_route_table.private_route_table.id}"
}

#######################################################################
